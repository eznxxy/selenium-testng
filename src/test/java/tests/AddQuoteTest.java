package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.QuoteHomePage;

import java.util.List;

public class AddQuoteTest {

    private ChromeDriver driver;

    @BeforeTest
    public void initialize() {
        //webdrivermanager
        WebDriverManager.chromedriver().setup();

        //chrome options
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");

        //init browser
        driver = new ChromeDriver(options);

        //set window max
        driver.manage().window().maximize();
    }

    @AfterTest
    public void destroySession() throws InterruptedException{
        Thread.sleep(5000);
        //close browser
        driver.quit();
    }

    @Test
    public void addQuoteWithColor() {
        QuoteHomePage homePage = new QuoteHomePage(driver);
        homePage.openPage();
        homePage.inputQuote("There is a will there is a way");
        homePage.selectColor("Yellow");
        homePage.clickButtonAddQuote();

        String actualData = homePage.getSecondQuote();

        Assert.assertEquals(actualData, "There is a will there is a way", "quote is not match");
    }

    @Test
    public void addQuoteWithBlueColor() {
        QuoteHomePage homePage = new QuoteHomePage(driver);
        homePage.openPage();
        homePage.inputQuote("You will learn what you see");
        homePage.selectColor("Blue");
        homePage.clickButtonAddQuote();

        String actualData = homePage.getSecondQuote();

        Assert.assertEquals(actualData, "You will learn what you see", "quote is not match");
    }
}
